/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_ui_service_extension_context.h"

#include <chrono>
#include <cstdint>

#include "ability_manager_client.h"
#include "ability_runtime/js_caller_complex.h"
#include "hilog_tag_wrapper.h"
#include "hilog_wrapper.h"
#include "js_extension_context.h"
#include "js_error_utils.h"
#include "js_data_struct_converter.h"
#include "js_runtime.h"
#include "js_runtime_utils.h"
#include "napi/native_api.h"
#include "napi_common_ability.h"
#include "napi_common_want.h"
#include "napi_common_util.h"
#include "napi_remote_object.h"
#include "napi_common_start_options.h"
#include "start_options.h"
#include "hitrace_meter.h"

namespace OHOS {
namespace AbilityRuntime {
namespace {

}// namespace

void JsUIServiceExtensionContext::Finalizer(napi_env env, void* data, void* hint)
{
    TAG_LOGD(AAFwkTag::UISERVC_EXT, "JsAbilityContext::Finalizer is called");
    std::unique_ptr<JsUIServiceExtensionContext>(static_cast<JsUIServiceExtensionContext*>(data));
}

napi_value JsUIServiceExtensionContext::StartAbility(napi_env env, napi_callback_info info)
{
    GET_NAPI_INFO_AND_CALL(env, info, JsUIServiceExtensionContext, OnStartAbility);
}

napi_value JsUIServiceExtensionContext::TerminateSelf(napi_env env, napi_callback_info info)
{
    GET_NAPI_INFO_AND_CALL(env, info, JsUIServiceExtensionContext, OnTerminateSelf);
}

napi_value JsUIServiceExtensionContext::StartAbilityByType(napi_env env, napi_callback_info info)
{
    GET_NAPI_INFO_AND_CALL(env, info, JsUIServiceExtensionContext, OnStartAbilityByType);
}

napi_value JsUIServiceExtensionContext::OnStartAbility(napi_env env, NapiCallbackInfo& info)
{
    return nullptr;
}

napi_value JsUIServiceExtensionContext::OnTerminateSelf(napi_env env, NapiCallbackInfo& info)
{
    napi_value result = nullptr;
    return result;
}

napi_value JsUIServiceExtensionContext::OnStartAbilityByType(napi_env, NapiCallbackInfo& info)
{
    napi_value result = nullptr;
    return result;
}

napi_value JsUIServiceExtensionContext::CreateJsUIServiceExtensionContext(napi_env env,
    std::shared_ptr<UIServiceExtensionContext> context)
{
    TAG_LOGD(AAFwkTag::UISERVC_EXT, "CreateJsUIServiceExtensionContext");
    std::shared_ptr<OHOS::AppExecFwk::AbilityInfo> abilityInfo = nullptr;
    if (context) {
        abilityInfo = context->GetAbilityInfo();
    }
    napi_value object = CreateJsExtensionContext(env, context, abilityInfo);

    std::unique_ptr<JsUIServiceExtensionContext> jsUiContext
        = std::make_unique<JsUIServiceExtensionContext>(context);
    napi_wrap(env, object, jsUiContext.release(), JsUIServiceExtensionContext::Finalizer, nullptr, nullptr);

    const char *moduleName = "JsUIServiceExtensionContext";
    BindNativeFunction(env, object, "startAbility", moduleName, JsUIServiceExtensionContext::StartAbility);
    BindNativeFunction(env, object, "terminateSelf", moduleName, JsUIServiceExtensionContext::TerminateSelf);
    BindNativeFunction(env, object, "startAbilityByType", moduleName,
        JsUIServiceExtensionContext::StartAbilityByType);
    return object;
}

} // namespace AbilityRuntime
}  // namespace OHOS